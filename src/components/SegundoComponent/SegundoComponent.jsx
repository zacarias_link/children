import { Component } from "react";

class SegundoComponent extends Component {
  render() {
    return (
      <div style={{ fontWeight: this.props.fontWeight }}>
        {this.props.title}
        {this.props.children}
      </div>
    );
  }
}

export default SegundoComponent;

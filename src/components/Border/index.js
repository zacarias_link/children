import { Component } from "react";
import "./styles.css";
import CorDoTexto from "../CorDoTexto/CorDoTexto.jsx";
import SegundoComponent from "../SegundoComponent/SegundoComponent.jsx";
class Border extends Component {
  render() {
    return (
      <div className={this.props.className}>
        <h1>{this.props.title}</h1>
        <div>{this.props.children}</div>
        <CorDoTexto
          title="primeiro teste"
          color="pink"
          title="primeiro teste"
        ></CorDoTexto>
        <SegundoComponent
          title="segundo teste"
          fontWeigth="bold"
          title="segundo teste"
        ></SegundoComponent>
      </div>
    );
  }
}

export default Border;

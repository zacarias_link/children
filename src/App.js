import logo from "./logo.svg";
import "./App.css";
import Border from "./components/Border";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Border title="hoje veremos o circo pegar fogo" className="Border">
          <div></div>
        </Border>
      </header>
    </div>
  );
}

export default App;
